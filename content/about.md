---
title: "About"
date: 2022-10-09T17:22:53-03:00
draft: false
---

Este site irá mostrar tudo que você precisa saber para não ser um completo racheiro, conceitos básicos de suspensão, dinâmica veicular e pneu.

![Suspensão de um Formula](https://cdn-1.motorsport.com/images/amp/2d1Xor3Y/s1000/mercedes-f1-w11-front-suspensi.webp)


Sobre mim

![Enzo Fogaça](https://media-exp1.licdn.com/dms/image/C4D22AQHhsSyxnWRoTw/feedshare-shrink_2048_1536/0/1663767440420?e=1668038400&v=beta&t=mum72xpE1jxw-yWrOHZr8KCjkUzl3m2w-hBEIQpMHZI)

Em 2016 iniciei meus estudos na ETEC Lauro Gomes, cursei técnico em mecatrônica integrado ao ensino médio. No ano de 2018 participei da Competição FESTO de mecatrônica e finalizei meus estudos na ETEC, como trabalho de conclusão de curso desenvolvi uma extrusora automatizada para a reciclagem de detritos de impressão 3D.
No ano de 2020 iniciei a graduação na Escola Politécnica da Universidade de São Paulo, onde atualmente faço bacharelado em engenharia mecatrônica, juntamente com a graduação, participo do grupo de extensão Equipe Poli Racing de Formula SAE no qual desenvolvi diferentes projetos, administração de projetos e membros de dinâmica veicular e atualmente desempenho o cargo de membro especialista de componentes estruturais, além de ser o responsável pela administração financeira junta ao fundo patrimonial Amigos da Poli.


![Enzo Fogaça AdP](https://media-exp1.licdn.com/dms/image/C4D22AQG2MM0wj-HqVA/feedshare-shrink_2048_1536/0/1663767440101?e=1668038400&v=beta&t=D53QNAezr2JN6xl3ks81ESnmucuyHW9MZtvGpvfneFo)